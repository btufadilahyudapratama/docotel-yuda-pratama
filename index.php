<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Test</title>
</head>

<body>
    <div class="container">
        <?php
// Nama-nama bayi
$listBayi = array("abichandra","bagas","binar","bintang","bulan","candra","damar","dimas","arif","subhan","wanda","dewangga","yuan","yudan","reza","daniel","syarif","briantomo","yesti","aida");

// NICU
$ruangMawar = array();
$ruangMelati = array();

foreach ($listBayi as $namaBayi) {
    $akhiran_huruf = strtolower(substr($namaBayi, -1));    
    if (in_array($akhiran_huruf, array('a', 'e', 'i', 'o', 'u'))) {
        // vocal, ruang Mawar
        $ruangMawar[] = $namaBayi;
    } else {
        // konsonan, ruang Melati
        $ruangMelati[] = $namaBayi;
    }
}
    ?>
        <h1>Ruang Mawaran</h1>
        <?php foreach ($ruangMawar as $bayiMawar) { ?>
        <button type="button" class="btn btn-light"><?= "$bayiMawar";?></button>
        <?php
    }
    ?>
        <h1>Ruang Melati</h1>
        <?php foreach ($ruangMelati as $bayiMelati) { ?>
        <button type="button" class="btn btn-light"><?= "$bayiMelati";?></button>
        <?php
    }
    ?>
    </div>
</body>

</html>